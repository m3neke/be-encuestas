const { PubSub } = require('apollo-server');
const { ApolloServer, gql } = require('apollo-server-express');
const express = require('express');
const mongoose = require('mongoose');
const { createServer } = require('http');

const User = require('./models/User');
const Iniciativa = require('./models/Iniciativa');
const Votacion = require('./models/Votacion');

const VOTACION_ADDED = 'VOTACION_ADDED';
const pubsub = new PubSub();
const app = express();

const typeDefs = gql`
  type Iniciativa {
    id: ID
    name: String
    product_owner: String
    saldo: Int
  }

  type Votacion {
    id: ID
    name: String
    product_owner: String
    saldo: Int
  }

  input iniciativa {
    id: ID
    name: String
    product_owner: String
    saldo: Int
  }

  type User {
    id: ID
    name: String
    password: String
    username: String
    saldo: Int
  }

  type LoginResponse {
    login: Boolean
    id: ID
  }

  type VotacionResponse {
    votacion: Boolean
  }

  type UpdateUserResponse {
    update: Boolean
  }

  type VotacionesResponse {
    iniciativa: String
    totalAmount: Int
    count: Int
  }

  type Subscription {
    votacionAdded: [VotacionesResponse]
  }

  type Query {
    iniciativas: [Iniciativa]
    getUser(id: ID): User
    votaciones: [VotacionesResponse]
  }

  type Mutation {
    addVotacion(votacion: [iniciativa]): VotacionResponse
    addIniciativa(name: String, product_owner: String, votes: Int): Iniciativa
    updateIniciativa(id: ID!, votes: Int!): Iniciativa
    login(user: String, password: String): LoginResponse
    updateSaldo(saldo: Int!): Boolean
    addUser(name: String, password: String, username: String, saldo: Int): User
    updateUser(id: ID): UpdateUserResponse
  }
`;

const resolvers = {
  Subscription: {
    votacionAdded: {
      subscribe: () => pubsub.asyncIterator([VOTACION_ADDED])
    }
  },
  Query: {
    async iniciativas() {
      try {
        let iniciativas = await Iniciativa.find({});
        return iniciativas
      } catch (e) {

      }
    },
    async getUser(root, args, context) {
      try {
        let found = await User.findOne({ _id: args.id });
        return found;
      } catch (e) {

      }
    },
    async votaciones() {
      try {
        let votaciones = await Votacion.aggregate(
          [
            {
              $group:
              {
                _id: { iniciativa: "$name" },
                totalAmount: { $sum: "$saldo" },
                count: { $sum: 1 },
              }
            },
            { $sort: { totalAmount: -1 } }
          ]
        );

        votaciones = votaciones.map(v => {
          return {
            iniciativa: v._id.iniciativa,
            totalAmount: v.totalAmount,
            count: v.count
          }
        });
        return votaciones;
      } catch (e) {
        console.log('error ', e);
      }
    }
  },
  Mutation: {
    async addVotacion(root, args, context) {
      try {
        let votacion = args.votacion.filter(v => v.saldo > 0);
        await Votacion.insertMany(votacion);
        let votaciones = await Votacion.aggregate(
          [
            {
              $group:
              {
                _id: { iniciativa: "$name" },
                totalAmount: { $sum: "$saldo" },
                count: { $sum: 1 },
              }
            },
            { $sort: { totalAmount: -1 } }
          ]
        );

        votaciones = votaciones.map(v => {
          return {
            iniciativa: v._id.iniciativa,
            totalAmount: v.totalAmount,
            count: v.count
          }
        });
        pubsub.publish(VOTACION_ADDED, { votacionAdded: votaciones });
        return {
          votacion: true
        }
      } catch (e) {
        return {
          votacion: false
        }
      }
    },
    async updateUser(root, args, context) {
      try {
        let user = await User.updateOne({ _id: args.id }, { saldo: 0 });
        if (user.nModified > 0) {
          return {
            update: true
          }
        }

        return {
          update: false
        }

      } catch (e) {

      }
    },
    addIniciativa(root, args, context) {
      iniciativas.push(args);
      return args;
    },
    updateIniciativa(root, args, context) {
      let found = iniciativas.find(e => e.id === args.id);
      return found;
    },
    async login(root, args, context) {
      try {
        let found = await User.findOne({ username: args.user });
        if (found) {
          if (found.password === args.password) {
            return {
              login: true,
              id: found._id
            }
          }
        }
        return {
          login: false
        }

      } catch (e) {

      }
    }
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: true,
  playground: true
});

server.applyMiddleware({ app, path:'/graphql'});

const httpServer = createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen( { port: process.env.PORT || 4000}, () => {
  console.log('Apollo server running');
});

mongoose.connect("mongodb://gnzlo:shimys123@ds251799.mlab.com:51799/negocioweb")
  .then(() => {
    console.log('connected BD');
  })
  .catch((err) => {
    console.log(`error ${err}`);
  });
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const votacionSchema = new Schema({
    name: String,
    product_owner: String,
    saldo: {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model('Votacion', votacionSchema);
